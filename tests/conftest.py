from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.database import get_db, Base
from app.config import settings
from app.main import app
from app.oauth2 import create_access_token
import pytest


@pytest.fixture
def test_user(no_auth_client):
    user_data = {
        'username': 'HeroNakamuro',
        'password': 'HardPassword',
        "money": 45000,
        "next_promotion_date": "2022-07-16"
    }
    response = no_auth_client.post('users', json=user_data)
    assert response.status_code == 201
    new_user = response.json()
    new_user['password'] = user_data['password']
    return new_user


@pytest.fixture
def no_auth_client():
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    yield TestClient(app)


@pytest.fixture
def token(test_user):
    return create_access_token(data={'user_id': test_user['id']})


@pytest.fixture
def auth_client(no_auth_client: TestClient, token):
    no_auth_client.headers = {
        **no_auth_client.headers,
        "Authorization": f"Bearer {token}"
    }
    return no_auth_client

# Config database test
SQLALCHEMY_DATABASE_URL = f'postgresql+psycopg://{settings.database_username}:{settings.database_password}@' \
                          f'{settings.database_hostname}:{settings.database_port}/{settings.database_name}_test'

engine = create_engine(SQLALCHEMY_DATABASE_URL)

TestSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    db = TestSessionLocal()
    try:
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db
