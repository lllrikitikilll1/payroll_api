import pytest
from app import schemas


def test_create_user(no_auth_client):
    """Тест создания пользователя без указания зарплаты и даты повышения"""

    data_user = {'username': 'BlackJack', 'password': '0000'}
    response = no_auth_client.post('/users', json=data_user)
    new_user = schemas.UserResponseValid(**response.json())
    assert response.status_code == 201
    assert new_user.username == data_user['username']
    assert new_user.money == 0
    assert new_user.next_promotion_date == None


def test_create_user_with_money(no_auth_client):
    """Тест создания пользователя с указанием зарплаты без даты повышения"""

    data_user = {'username': 'BlackJack', 'password': '0000', 'money': 45000}
    response = no_auth_client.post('/users', json=data_user)
    new_user = schemas.UserResponseValid(**response.json())
    assert response.status_code == 201
    assert new_user.username == data_user['username']
    assert new_user.money == data_user['money']
    assert new_user.next_promotion_date == None


def test_create_user_with_next_promotion_date(no_auth_client):
    """Тест создания пользователя с указанием даты повышения, без зарплаты"""

    data_user = {'username': 'BlackJack', 'password': '0000', 'next_promotion_date': '2022-07-16'}
    response = no_auth_client.post('/users', json=data_user)
    new_user = schemas.UserResponseValid(**response.json())
    assert response.status_code == 201
    assert new_user.username == data_user['username']
    assert new_user.money == 0
    assert str(new_user.next_promotion_date) == data_user['next_promotion_date']


def test_create_user_with_all_param(no_auth_client):
    """Тест создания пользователя с указанием зарплаты и даты повышения"""
    data_user = {'username': 'BlackJack', 'password': '0000', 'money': 45000,
                 'next_promotion_date': '2022-07-16'}
    response = no_auth_client.post('/users', json=data_user)
    new_user = schemas.UserResponseValid(**response.json())
    assert response.status_code == 201
    assert new_user.username == data_user['username']
    assert new_user.money == data_user['money']
    assert str(new_user.next_promotion_date) == data_user['next_promotion_date']


@pytest.mark.parametrize("username, password, money, next_promotion_date", [
    ("BlackJack", '0000', 'wordmoney', '2022-07-16'),
    (None, '0000', 45000, '2022-07-16'),
    ("BlackJack", None, 45000, '2022-07-16'),
    ("BlackJack", '0000', 45000, '2022/07/16'),
])
def test_incorrect_create_user(no_auth_client, username, password, money, next_promotion_date):
    """Тест создания пользователя с некорректными данными"""

    response = no_auth_client.post('/users', json={'username': username, 'password': password, 'money': money,
                                                   'next_promotion_date': next_promotion_date})
    assert response.status_code == 422
