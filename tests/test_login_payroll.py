import pytest
from jose import jwt
from app import schemas
from app.config import settings


def test_login_user(no_auth_client, test_user):
    """Тест аутентификации пользователя"""

    response = no_auth_client.post('/login', data={'username': test_user["username"],
                                                   "password": test_user["password"]})
    login_response = schemas.TokenValid(**response.json())  # получаем токен после входа
    payload = jwt.decode(login_response.access_token,
                         settings.secret_key,
                         algorithms=[settings.algorithm])  # декодируем токен
    id = payload.get('user_id')  # Берем из декодированного токена полезные данные
    assert response.status_code == 200
    assert login_response.token_type == 'bearer'
    assert id == test_user['id']


@pytest.mark.parametrize('username, password, status_code', [
    ("WrongUsername", "HardPassword", 403),
    ("HeroNakamuro", "WrongPassword", 403),
    ("WrongUsername", "WrongPassword", 403),
    ("HeroNakamuro", None, 422),
    (None, "HardPassword", 422),
])
def test_incorrect_user(no_auth_client, username, password, status_code):
    """Тест аутентификации пользователя с некорректными данными"""

    response = no_auth_client.post('/login', data={'username': username,
                                                   "password": password})
    assert response.status_code == status_code


def test_get_payroll_auth_user(auth_client, test_user):
    """Запрос данных зарплаты и даты повышения аутентифицированного пользователя"""

    response = auth_client.get('/payroll')
    response_payroll = schemas.PayrollResponceValid(**response.json())
    assert response.status_code == 200
    assert response_payroll.money == test_user['money']
    assert str(response_payroll.next_promotion_date) == test_user['next_promotion_date']


def test_get_payroll_no_auth_user(no_auth_client, test_user):
    """Запрос данных зарплаты и даты повышения НЕ аутентифицированного пользователя"""

    response = no_auth_client.get('/payroll')
    assert response.status_code == 401
    assert response.json().get('detail') == 'Not authenticated'