from typing import Optional
from pydantic import BaseModel
import datetime


class UserCreateValid(BaseModel):  # Валидация входных данных на создание пользователя
    username: str
    password: str
    money: int = 0
    next_promotion_date: datetime.date = None


class UserResponseValid(BaseModel):  # Валидация ответа пользователю на "создание нового пользователя"
    id: int
    username: str
    money: int
    next_promotion_date: datetime.date | None = None

    class Config:
        orm_mode = True


class PayrollResponceValid(BaseModel):  # Валидация ответа зарплаты и даты следующего повышения
    money: int
    next_promotion_date: datetime.date | None

    class Config:
        orm_mode = True


class TokenValid(BaseModel):  # Валидация ответа получения пользователем токена
    access_token: str
    token_type: str


class TokenDataValid(BaseModel):  # Валидация расшифрованного токена
    id: Optional[int] = None
