from fastapi import FastAPI, Depends, HTTPException, status
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from . import models, schemas, oauth2, utils, database


models.Base.metadata.create_all(bind=database.engine)

app = FastAPI(title='Payroll ЗП и дата повышения')


@app.post('/users', status_code=status.HTTP_201_CREATED, response_model=schemas.UserResponseValid)
def create_user(user: schemas.UserCreateValid, db: Session = Depends(database.get_db)):
    """Создание нового пользователя"""

    hashed_password = utils.hash_password(user.password)
    user.password = hashed_password

    new_user = models.Users(**user.dict())
    db.add(new_user)
    try:
        db.commit()
    except:
        db.rollback()
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Такой пользователь уже существует")

    db.refresh(new_user)
    return new_user


@app.post('/login', response_model=schemas.TokenValid)
def login(user_credentials: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(database.get_db)):
    """Аутентификация пользователя и выдача токена"""

    user = db.query(models.Users).filter(
        models.Users.username == user_credentials.username).first()

    if not user:  # Проверка существования пользователя в БД
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Неверные учетные данные')

    if not utils.verify(user_credentials.password, user.password):  # Проверка пароля
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Неверные учетные данные')
    access_token = oauth2.create_access_token(data={'user_id': user.id})

    return {'access_token': access_token, "token_type": "bearer"}  # возвращает токен


@app.get("/payroll", response_model=schemas.PayrollResponceValid)
def get_payroll(db: Session = Depends(database.get_db), current_user: int = Depends(oauth2.get_current_user)):
    """Просмотр зарплаты и повышения аутентифицированного пользователя"""

    payroll = db.query(models.Users).filter(models.Users.id == current_user.id).first()
    return payroll
