from sqlalchemy import CheckConstraint, Column, Integer, String
from sqlalchemy.sql.sqltypes import Date
from .database import Base


class Users(Base):  # Класс/таблица
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, nullable=False)  # Уникальный id пользователя
    username = Column(String, nullable=False, unique=True)  # Логин пользователя для входа
    password = Column(String, nullable=False)  # Хешируемый пароль
    money = Column(Integer, nullable=False)  # Заработная плата на данный момент
    __table_args__ = (
        CheckConstraint(money >= 0, name='positive_money_constraint'),
    )
    next_promotion_date = Column(Date(), nullable=True)  # Дата следующего повышения
